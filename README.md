# Pygments lexer for `dig` output

This is **work in progress** and probably quite horrible.

## What?

This is a [lexer][lexer] for [Pygments][pygments] that can lex the output of [BIND 9's `dig` command][dig] to provide colorful syntax highlighting for `dig`:

![Screenshot of colorful dig output][screenshot]

## Why?

`dig` output can be hard to read, especially with `+trace`.  Having some colors in the output helps a lot when reading large dig responses.

## How to use?

Put something like this in your shell's config, e.g. `.bashrc`, `.zshrc`, or whatever your shell uses:

```bash
function dig() {
    if [[ -t 1 || "$DIG_COLOR" == "always" ]]; then
        /usr/bin/dig $@ | pygmentize -F tokenmerge -xl path/to/dig-lexer.py -Ostyle=monokai
    else
        /usr/bin/dig $@
    fi
}
```

Then rehash your shell config or start a new session.  Now calling `dig` will call this function instead, passing the arguments to the original `dig` command, e.g. in `/usr/bin/dig`, and pipe the output through `pygmentize`.

If necessary, you can still call the original dig by its full path, e.g. `/usr/bin/dig` instead of just `dig`.

[screenshot]: https://gitlab.com/s3lph/pygments-dig-lexer/-/raw/master/screenshot.png
[lexer]: https://en.wikipedia.org/wiki/Lexical_analysis
[pygments]: https://pygments.org/
[dig]: https://en.wikipedia.org/wiki/Dig_(command)